//
//  Bishop.swift
//  final-project
//
//  Created by Roman Alikevich on 09.04.2021.
//

import UIKit

class Bishop: ChessPiece {
    func isEndPoint(orignalView: UIView, currentView: UIView) -> Bool {
        return false
    }
    
    func isEatablePiece(capturedPiece: ChessBoardCell, currentPiece: ChessBoardCell) -> Bool {
        return false
    }
    
    
    var color: ChessPieceColor?
    var sizeBoard: CGFloat = 0.0
    var shift: CGFloat = 0.0
    
    public init(color: ChessPieceColor?) {
        self.color = color
    }
    
    func isCurrentPieceStep(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool {
        return true
    }
    
    func getShiftByColor(shift: CGFloat) -> CGFloat {
        if color == ChessPieceColor.white {
            return -shift
        }
        return shift
    }
    
    func isLeftUpStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        let size = 8
        for i in 1..<size {
            if originalPointX - shift * CGFloat(i) == currentView.frame.minX &&
                originalPointY - shift * CGFloat(i) == currentView.frame.minY {
                return true
            }
        }
        return false
    }
    
    func isRightUpStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        let size = 8
        for i in 1..<size {
            if originalPointX + shift * CGFloat(i) == currentView.frame.minX &&
                originalPointY - shift * CGFloat(i) == currentView.frame.minY {
                return true
            }
        }
        return false
    }
    
    func isLeftDownStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        let size = 8
        for i in 1..<size {
            if currentView.frame.minX == 100 && currentView.frame.minY == 300 {
                print("d")
            }
            if originalPointX - shift * CGFloat(i) == currentView.frame.minX &&
                originalPointY + shift * CGFloat(i) == currentView.frame.minY {
                return true
            }
        }
        return false
    }
    
    func isRightDownStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        let size = 8
        for i in 1..<size {
            if originalPointX + shift * CGFloat(i) == currentView.frame.minX &&
                originalPointY + shift * CGFloat(i) == currentView.frame.minY {
                return true
            }
        }
        return false
    }
    
    func isBishopShift(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        if isRightDownStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) ||
            isLeftDownStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) ||
            isRightUpStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) ||
            isLeftUpStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) {
            return true
        }
        return false
    }
    
    func getPerhapsMoveArray(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat)) -> [UIView] {
        
        var pieceShiftArray: [UIView] = []
        sizeBoard = ChessWareHouse.shared.chessBoardWidth ?? 400.0
        self.shift = 50
        print(ChessWareHouse.shared.chessPieceStorage.count)
        for subview in ChessWareHouse.shared.chessPieceStorage {
            guard let currentView = subview as? ChessBoardCell else {
                continue
            }
            if isRightDownStep(currentView: currentView, originalPointX: orignalMinimalPoint.0, originalPointY: orignalMinimalPoint.1) {
                pieceShiftArray.append(subview)
            }
            if isLeftDownStep(currentView: currentView, originalPointX: orignalMinimalPoint.0, originalPointY: orignalMinimalPoint.1) {
                pieceShiftArray.append(subview)
            }
            if isRightUpStep(currentView: currentView, originalPointX: orignalMinimalPoint.0, originalPointY: orignalMinimalPoint.1) {
                pieceShiftArray.append(subview)
            }
            if isLeftUpStep(currentView: currentView, originalPointX: orignalMinimalPoint.0, originalPointY: orignalMinimalPoint.1) {
                pieceShiftArray.append(subview)
            }
        }
    return pieceShiftArray
}

//    func isCorrectMove(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool {
//        return false
//    }

func movePiece(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat), pieceShiftArray: [UIView]) {
}
}
