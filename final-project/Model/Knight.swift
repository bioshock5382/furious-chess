//
//  Knight.swift
//  final-project
//
//  Created by Roman Alikevich on 09.04.2021.
//

import UIKit

class Knight: ChessPiece {
    func isEndPoint(orignalView: UIView, currentView: UIView) -> Bool {
        return false
    }
    
    func isEatablePiece(capturedPiece: ChessBoardCell, currentPiece: ChessBoardCell) -> Bool {
        return false
    }
    
   
    var color: ChessPieceColor?
    var sizeBoard: CGFloat = 0.0
    var shift: CGFloat = 0.0
    
    public init(color: ChessPieceColor?) {
            self.color = color
    }
    
    func isCurrentPieceStep(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool {
        return true
    }
    

    func getShiftByColor(shift: CGFloat) -> CGFloat {
        if color == ChessPieceColor.white {
            return -shift
        }
        return shift
    }
    
    func isLeftStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        return currentView.frame.minX - shift * 2 == originalPointX &&
            (currentView.frame.minY == originalPointY - shift ||
                currentView.frame.minY == originalPointY + shift)
    }
    
    func isRightStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        return currentView.frame.minX + shift * 2 == originalPointX &&
            (currentView.frame.minY == originalPointY - shift ||
                currentView.frame.minY == originalPointY + shift)
    }
    
    func isAboveStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        return currentView.frame.minY + shift * 2 == originalPointY &&
            (currentView.frame.minX == originalPointX - shift ||
                currentView.frame.minX == originalPointX + shift)
    }
    
    func isBelowStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        return currentView.frame.minY - shift * 2 == originalPointY &&
            (currentView.frame.minX == originalPointX - shift ||
                currentView.frame.minX == originalPointX + shift)
    }
    
    func isKnightShift(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        if isAboveStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) ||
            isBelowStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) ||
            isRightStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) ||
            isLeftStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) {
            return true
        }
        
        return false
    }
    
    func getPerhapsMoveArray(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat)) -> [UIView] {
        print("knight getPerhapsMoveArray")
        var pieceShiftArray: [UIView] = []
        sizeBoard = ChessWareHouse.shared.chessBoardWidth ?? 400.0
        self.shift = getShiftByColor(shift: ChessWareHouse.shared.chessBoardWidth! / 8)
        print(ChessWareHouse.shared.chessPieceStorage.count)
            for subview in ChessWareHouse.shared.chessPieceStorage {
                guard let currentView = subview as? ChessBoardCell else {
                    continue
                }
//              self.shift = getShiftByColor(shift: ChessWareHouse.shared.chessBoardWidth! / 8)
                
                if isKnightShift(currentView: subview, originalPointX: orignalMinimalPoint.0, originalPointY: orignalMinimalPoint.1) && (subview.frame.maxY - subview.frame.minY) != ChessWareHouse.shared.chessBoardWidth {
                    if currentView.color != self.color && currentView.boardCell == ChessBoardCellType.empty {
                        pieceShiftArray.append(subview)
                    } else if currentView.color != self.color && currentView.boardCell == ChessBoardCellType.full {
                        pieceShiftArray.append(subview)
                    }
              }
            }
        return pieceShiftArray
    }
    
//    func isCorrectMove(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool {
//        return false
//    }
    
    func movePiece(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat), pieceShiftArray: [UIView]) {
    }
}
