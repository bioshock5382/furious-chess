//
//  PieceEntity.swift
//  final-project
//
//  Created by Roman Alikevich on 09.04.2021.
//

import UIKit

//enum PieceError : Error, RawRepresentable {
//
//    var rawValue: Error.Type
//
//    init?(rawValue: Error) {
//        throw Error()
//    }
//
//    typealias RawValue = Error.Type
//
//    case invalidValue
//}
enum PieceEntity : String {
//    typealias invalidValue = PieceError.Type
    case pawn = "p"
    case bishop = "b"
    case rook = "r"
    case king = "k"
    case queen = "q"
    case knight = "n"
    case invalidValue = "invalid"
    case empty = "empty"
}
