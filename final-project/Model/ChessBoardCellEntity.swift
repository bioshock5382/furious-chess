//
//  ChessBoardCellEntity.swift
//  final-project
//
//  Created by Roman Alikevich on 04.04.2021.
//

import UIKit

enum ChessBoardCellType {
    case empty
    case full
}

enum ChessPieceColor {
    case white
    case black
    case transparent
}

class ChessBoardCellEntity: NSObject {
    var pieceImage: UIImage?
    var pieceName: PieceEntity?
    var color: ChessPieceColor?
    var boardCell: ChessBoardCellType?
    
    init(pieceImage: UIImage?, pieceName: PieceEntity?, color: ChessPieceColor?, boardCell: ChessBoardCellType) {
        self.pieceImage = pieceImage
        self.pieceName = pieceName
        self.color = color
        self.boardCell = boardCell
    }
}
