//
//  Queen.swift
//  final-project
//
//  Created by Roman Alikevich on 09.04.2021.
//

import UIKit

class Queen: ChessPiece {
    func isEndPoint(orignalView: UIView, currentView: UIView) -> Bool {
        print("Queen-isEndPoint")
        return false
    }
    
    func isEatablePiece(capturedPiece: ChessBoardCell, currentPiece: ChessBoardCell) -> Bool {
        return false
    }
    
    
    var color: ChessPieceColor?
    
    public init(color: ChessPieceColor?) {
            self.color = color
    }
    
    func isCurrentPieceStep(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool {
        return true
    }
    
    func deselectViewArray(viewList: [UIView]) {
        
    }
    
    func selectViewArray(viewList: [UIView]) {
        
    }
    
    func getPerhapsMoveArray(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> [UIView] {
        print("queen getPerhapsMoveArray")
        return [UIView()]
    }
    
//    func isCorrectMove(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool {
//        return false
//    }
    
    func movePiece(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat), pieceShiftArray: [UIView]) {
    }
}
