//
//  Rook.swift
//  final-project
//
//  Created by Roman Alikevich on 09.04.2021.
//

import UIKit

class Rook: ChessPiece {
    func isEndPoint(orignalView: UIView, currentView: UIView) -> Bool {
        return false
    }
    
    
    //    func isCorrectMove(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool {
    //        return false
    //    }
    
    func movePiece(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat), pieceShiftArray: [UIView]) {
        
    }
    
    
    var color: ChessPieceColor?
    var sizeBoard: CGFloat = 0.0
    var shift: CGFloat = 0.0
    
    public init(color: ChessPieceColor?) {
        self.color = color
    }
    
    func isCurrentPieceStep(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool {
        return true
    }
    
    func getShiftByColor(shift: CGFloat) -> CGFloat {
        if color == ChessPieceColor.white {
            return -shift
        }
        return shift
    }
    
    func isLeftStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        let size = 9
        for i in 1..<size {
            if originalPointX + shift * CGFloat(i) == currentView.frame.minX &&
                originalPointY == currentView.frame.minY {
                return true
            }
        }
        return false
    }
    
    func isRightStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        let size = 9
        for i in 1..<size {
            if originalPointX - shift * CGFloat(i) == currentView.frame.minX &&
                originalPointY == currentView.frame.minY {
                return true
            }
        }
        return false
    }
    
    func isAboveStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        let size = 9
        for i in 1..<size {
            if originalPointY + shift * CGFloat(i) == currentView.frame.minY &&
                originalPointX == currentView.frame.minX {
                return true
            }
        }
        return false
    }
    
    func isBelowStep(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        let size = 9
        for i in 1..<size {
            if originalPointY - shift * CGFloat(i) == currentView.frame.minY &&
                originalPointX == currentView.frame.minX {
                return true
            }
        }
        return false
    }
    
//    func isBishopShift(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
//
//        if isAboveStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) ||
//            isBelowStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) ||
//            isRightStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) ||
//            isLeftStep(currentView: currentView, originalPointX: originalPointX, originalPointY: originalPointY) {
//
//            return true
//        }
//        return false
//    }
    
    func isEatablePiece(capturedPiece: ChessBoardCell, currentPiece: ChessBoardCell) -> Bool {
        return self.color != currentPiece.color
    }
    
    func getPerhapsMoveArray(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat)) -> [UIView] {
        
        var pieceShiftArray: [UIView] = []
        sizeBoard = ChessWareHouse.shared.chessBoardWidth ?? 400.0
        self.shift = -getShiftByColor(shift: ChessWareHouse.shared.chessBoardWidth! / 8)
        var resultArray: [CGFloat] = [CGFloat](repeating: 0.0, count: 4)
        print(ChessWareHouse.shared.chessPieceStorage.count)
        for subview in ChessWareHouse.shared.chessPieceStorage {
            guard let currentView = subview as? ChessBoardCell else {
                continue
            }
//            if type(of: subview) == "UIImageView" {
//            var currentView = subview as! ChessBoardCell

            if (subview.frame.maxY - subview.frame.minY) != ChessWareHouse.shared.chessBoardWidth {
                if isAboveStep(currentView: subview, originalPointX: orignalMinimalPoint.0, originalPointY: orignalMinimalPoint.1) && orignalMinimalPoint.1 != 0 {
                    if orignalMinimalPoint.1 == 0.0 {
                        resultArray[1] = 0.0
                    } else {
                    if currentView.boardCell == ChessBoardCellType.full &&
                        currentView.color == self.color && abs(resultArray[0]) <= abs(subview.frame.minY) {
                        resultArray[0] = subview.frame.minY + subview.frame.width
                    } else if currentView.boardCell == ChessBoardCellType.full &&
                                currentView.color != self.color && abs(resultArray[0]) <= abs(subview.frame.minY) {
                        resultArray[0] = subview.frame.minY + subview.frame.width
                    } else if currentView.boardCell == ChessBoardCellType.empty &&
                                currentView.color != self.color && abs(resultArray[0]) >= abs(subview.frame.minY) {
                        resultArray[0] = subview.frame.minY - subview.frame.width
                    }
                    }
                    pieceShiftArray.append(subview)
                }
                if isBelowStep(currentView: subview, originalPointX: orignalMinimalPoint.0, originalPointY: orignalMinimalPoint.1) {
                    if orignalMinimalPoint.1 == 350.0 {
                        resultArray[1] = 350.0
                    } else {
                    if currentView.boardCell == ChessBoardCellType.full &&
                        currentView.color == self.color &&  abs(resultArray[1]) <= abs(subview.frame.minY) {
                        resultArray[1] = subview.frame.minY - subview.frame.width
                    } else if currentView.boardCell == ChessBoardCellType.empty &&
                                currentView.color != self.color && abs(resultArray[1]) <= abs(subview.frame.minY) {
                        resultArray[1] = subview.frame.minY //- subview.frame.width
                    } else if currentView.boardCell == ChessBoardCellType.full &&
                                currentView.color != self.color && abs(resultArray[1]) <= abs(subview.frame.minY) {
                        resultArray[1] = subview.frame.minY - subview.frame.width
                    }
                    }
                    pieceShiftArray.append(subview)
                }
                if isRightStep(currentView: subview, originalPointX: orignalMinimalPoint.0, originalPointY: orignalMinimalPoint.1) {
                    print("isRightStep")
                    if orignalMinimalPoint.0 == 350.0 {
                        resultArray[2] = 350.0
                    } else {
                    if currentView.boardCell == ChessBoardCellType.full &&
                        currentView.color == self.color &&  abs(resultArray[2]) >= abs(subview.frame.minX) {
                        resultArray[2] = subview.frame.minX - subview.frame.width
//                        print("\(resultArray[2]) was changed on \(resultArray[2])")
                    } else if currentView.boardCell == ChessBoardCellType.empty &&
                                currentView.color != self.color {
                        resultArray[2] = subview.frame.minX
                    } else if currentView.boardCell == ChessBoardCellType.full &&
                                currentView.color != self.color && abs(resultArray[2]) >= abs(subview.frame.minX) {
                        resultArray[2] = subview.frame.minX + subview.frame.width
                    }
                    }
                    
                    pieceShiftArray.append(subview)
                }
                if isLeftStep(currentView: subview, originalPointX: orignalMinimalPoint.0, originalPointY: orignalMinimalPoint.1) {
                    print("isLeftStep")
                    if orignalMinimalPoint.0 == 0.0 {
                        resultArray[3] = 0.0
                    } else {
                    if currentView.boardCell == ChessBoardCellType.full &&
                        currentView.color == self.color && abs(resultArray[3] - orignalMinimalPoint.0) <=      abs(subview.frame.minX - orignalMinimalPoint.0) {
                        resultArray[3] = subview.frame.minX + subview.frame.width
                    } else if currentView.boardCell == ChessBoardCellType.empty &&
                                currentView.color != self.color && abs(resultArray[3] - orignalMinimalPoint.0) <=      abs(subview.frame.minX - orignalMinimalPoint.0){
                        resultArray[3] = subview.frame.minX
                    } else if currentView.boardCell == ChessBoardCellType.full &&
                                currentView.color != self.color && abs(resultArray[3] - orignalMinimalPoint.0) <=      abs(subview.frame.minX - orignalMinimalPoint.0) {
                                resultArray[3] = subview.frame.minX
                    }
                    }
                    pieceShiftArray.append(subview)
                }
                
            }
        }
        var bufferArray: [UIView] = []
    
        for piece in pieceShiftArray {
            if resultArray[0] != 0.0 && piece.frame.minY >= resultArray[0] && piece.frame.minY == orignalMinimalPoint.1 {
                bufferArray.append(piece)
            } else if resultArray[1] != 0.0 && piece.frame.minY <= resultArray[1] && piece.frame.minY == orignalMinimalPoint.1 {
                bufferArray.append(piece)
            } else if resultArray[2] != 0.0 && piece.frame.minX >= resultArray[2] && piece.frame.minX == orignalMinimalPoint.0 {
                bufferArray.append(piece)
            } else if resultArray[3] != 0.0 && piece.frame.minX <= resultArray[3] && piece.frame.minX == orignalMinimalPoint.0 {
                bufferArray.append(piece)
            }
        }
        
        
           
                
            

       
//
//        if resultArray[0] == 0.0 && resultArray[1] == 0.0 &&
//                    resultArray[2] == 0.0 && resultArray[3] == 0.0 {
//            return []
//        } else if resultArray[2] == 0.0 && resultArray[3] == 0.0 {
//            for piece in pieceShiftArray {
//                if resultArray[1] == 0.0 && piece.frame.minY >= resultArray[0] {
//                    bufferArray.append(piece)
//                } else if resultArray[0] == 0.0 && piece.frame.minY <= resultArray[1] {
//                    bufferArray.append(piece)
//                } else if resultArray[1] != 0.0 && resultArray[0] != 0.0 &&
//                            piece.frame.minY <= resultArray[1] && piece.frame.minY >= resultArray[0] {
//                    bufferArray.append(piece)
//                }
//            }
//        } else if resultArray[0] == 0.0 && resultArray[1] == 0.0 {
//            for piece in pieceShiftArray {
//                 if resultArray[2] == 0.0 && piece.frame.minY <= resultArray[3] {
//                      bufferArray.append(piece)
//                 } else if resultArray[3] == 0.0 && piece.frame.minY >= resultArray[2] {
//                      bufferArray.append(piece)
//                } else if resultArray[2] != 0.0 && resultArray[3] != 0.0 &&
//                            piece.frame.minY <= resultArray[3] && piece.frame.minY >= resultArray[2] {
//                    bufferArray.append(piece)
//                }
//    }
//        }
        return bufferArray
    }
}
