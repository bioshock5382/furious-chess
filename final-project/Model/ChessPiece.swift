//
//  ChessPiece.swift
//  final-project
//
//  Created by Roman Alikevich on 09.04.2021.
//

import UIKit

protocol ChessPiece {
    
    var color: ChessPieceColor? { get }
//    var sizeBoard: CGFloat = 0.0
//    var shift: CGFloat = 0.0

    func getPerhapsMoveArray(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> [UIView]
    func isCurrentPieceStep(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool
    func isEatablePiece(capturedPiece: ChessBoardCell, currentPiece: ChessBoardCell) -> Bool
    func isEndPoint(orignalView: UIView, currentView: UIView) -> Bool
//    func isCorrectMove(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool
    func movePiece(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat), pieceShiftArray: [UIView])
}

extension ChessPiece {
    func isBoardView(view: UIView) -> Bool {
        return view.frame.maxX - view.frame.minX != ChessWareHouse.shared.chessBoardWidth
    }
    
    func isExistViewByCoordinate(x: CGFloat, y: CGFloat, view: UIView) -> Bool {
        return (x >= view.frame.minX + (view.frame.maxX - view.frame.minX) / 4) &&
            (y >= view.frame.minY + (view.frame.maxX - view.frame.minX) / 4) &&
            x < view.frame.maxX + (view.frame.maxX - view.frame.minX) / 4 &&
            y < view.frame.maxY + (view.frame.maxX - view.frame.minX) / 4
    }
    
    func isExistViewByCoordinateShortPart(x: CGFloat, y: CGFloat, view: UIView) -> Bool {
        return x <= view.frame.minX &&
            y <= view.frame.minY &&
            x < view.frame.maxX && y < view.frame.maxY
    }
    
//    func isAlreadyPlacedPiece(x: CGFloat, y: CGFloat, view: UIView) -> Bool {
//        return false
//    }
    
    func findViewByСoordinate(_ x: CGFloat, _ y: CGFloat) -> UIView {
        var view: UIView?
        var flag = 0
        for subview in ChessWareHouse.shared.chessPieceStorage {
            flag += 1
            if x >= subview.frame.minX && y >= subview.frame.minY &&
                x < subview.frame.maxX && y < subview.frame.maxY &&
                subview.frame.maxX - subview.frame.minX != ChessWareHouse.shared.chessBoardWidth {
                view = subview
            }
        }
        return view ?? UIView()
    }
    
//    func isExistViewNearByOriginalView(originalPointX: CGFloat, originalPointY: CGFloat, view: UIView) -> Bool {
////        print(view)
//        return originalPointX >= view.frame.minX - (view.frame.maxX - view.frame.minX) / 2 && originalPointY >= view.frame.minY - (view.frame.maxY - view.frame.minY) / 2 &&
//            originalPointX <= view.frame.maxX + (view.frame.maxX - view.frame.minX) / 2 && originalPointY <= view.frame.maxY + (view.frame.maxY - view.frame.minY) / 2
//    }
//
//    func isExistViewNearByOriginalFinalPoint(originalPointX: CGFloat, originalPointY: CGFloat, x: CGFloat, y: CGFloat, view: UIView) -> Bool {
//        return originalPointX >= x - (view.frame.maxX - view.frame.minX) / 2 && originalPointY >= y - (view.frame.maxY - view.frame.minY) / 2 &&
//            originalPointX <= x + (view.frame.maxX - view.frame.minX) / 2 && originalPointY <= y + (view.frame.maxY - view.frame.minY) / 2
//    }
    
    func deselectViewArray(viewList: [UIView]) {
        for currentView in viewList {
            currentView.alpha = 1
            currentView.backgroundColor = .clear
        }
    }
    
    func selectViewArray(viewList: [UIView]) {
        for currentView in viewList {
            currentView.backgroundColor = .red
            currentView.alpha = 0.6
        }
    }
    
    func isCorrectMove(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat)) -> Bool {
        var pieceShiftArray: [UIView] = []
//        let sizeBoard = ChessWareHouse.shared.chessBoardWidth ?? 400.0
        
        var finalView = findViewByСoordinate(finalPointX, finalPointY)

        if !isExistViewByCoordinate(x: finalPointX, y: finalPointY, view: finalView) {
            finalView.frame = CGRect(x: orignalMinimalPoint.0, y: orignalMinimalPoint.1, width: finalView.frame.maxX - finalView.frame.minX, height: finalView.frame.maxX - finalView.frame.minX)
        } else {
            pieceShiftArray = getPerhapsMoveArray(x: finalPointX, y: finalPointY, orignalMinimalPoint)
        }
        var isFound = false
        for validView in pieceShiftArray {
      
        if isExistViewByCoordinate(x: finalPointX, y: finalPointY, view: validView) {
            finalView.frame = CGRect(x: validView.frame.minX, y: validView.frame.minY, width: validView.frame.maxX - validView.frame.minX, height: validView.frame.maxX - validView.frame.minX)
            isFound = true
        }
        }
        if !isFound {
                finalView.frame = CGRect(x: orignalMinimalPoint.0, y: orignalMinimalPoint.1, width: finalView.frame.maxX - finalView.frame.minX, height: finalView.frame.maxX - finalView.frame.minX)
            }
        return false
    }
    
    func isWhitePiece() -> Bool {
        return color == ChessPieceColor.white
    }
}
