//
//  InvalidChessPieceError.swift
//  final-project
//
//  Created by Roman Alikevich on 09.04.2021.
//

import UIKit

class InvalidChessPieceError: ChessPiece, Error {
    func isEndPoint(orignalView: UIView, currentView: UIView) -> Bool {
        return false
    }
    
    func isEatablePiece(capturedPiece: ChessBoardCell, currentPiece: ChessBoardCell) -> Bool {
        return false
    }
    
    
    var color: ChessPieceColor?
    
    func isCurrentPieceStep(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool {
        return false
    }
    
    func deselectViewArray(viewList: [UIView]) {
        
    }
    
    func selectViewArray(viewList: [UIView]) {
        
    }
    
    func getPerhapsMoveArray(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> [UIView] {
        print("invalid getPerhapsMoveArray")
        return [UIView()]
    }
    
//    func isCorrectMove(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool {
//        return false
//    }
    
    func movePiece(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat), pieceShiftArray: [UIView]) {
//        throw Error("")
    }
    
    
}
