//
//  Pawn.swift
//  final-project
//
//  Created by Roman Alikevich on 09.04.2021.
//

import UIKit

class Pawn: ChessPiece {
    
    var color: ChessPieceColor?
    var sizeBoard: CGFloat = 0.0
    var shift: CGFloat = 0.0
    
    public init(color: ChessPieceColor?) {
            self.color = color
    }
    
    func getShiftByColor(shift: CGFloat) -> CGFloat {
        if color == ChessPieceColor.white {
            return -shift
        }
        return shift
    }
    
    func isDoubleShift(view: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        return originalPointY - self.shift * 2 == view.frame.minY &&
            originalPointX == view.frame.minX
    }
    
    func isSingleShift(view: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
       return originalPointY - (view.frame.maxY - view.frame.minY) == view.frame.minY &&
            (originalPointX >= view.frame.minX - self.shift && originalPointX <= view.frame.maxX)
    }
    
    func isDoubleWhiteShift(view: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        return originalPointY - self.shift * 2 == view.frame.minY &&
            originalPointX == view.frame.minX
    }
    
    func isSingleWhiteShift(view: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        return originalPointY - self.shift == view.frame.minY &&
            (originalPointX <= view.frame.minX - self.shift && originalPointX >= view.frame.minX + self.shift)
    }
    
    func isPawnShift(currentView: UIView, originalPointX: CGFloat, originalPointY: CGFloat) -> Bool {
        
        if (sizeBoard / 8 >= originalPointY || sizeBoard * 3 / 4 <= originalPointY) &&
            isWhitePiece() &&
            (currentView.frame.maxY - currentView.frame.minY) == currentView.frame.width &&
            originalPointY - self.shift != 0 {
            if isDoubleWhiteShift(view: currentView, originalPointX: originalPointX, originalPointY: originalPointY) || isSingleWhiteShift(view: currentView, originalPointX: originalPointX, originalPointY: originalPointY) {
//                currentView.backgroundColor = .red
//                currentView.alpha = 0.6
                return true
            }
        } else if (sizeBoard * 3 / 4 <= originalPointY || sizeBoard / 8 >= originalPointY) &&
                    !isWhitePiece() &&
                    (currentView.frame.maxY - currentView.frame.minY) == currentView.frame.width &&
                    originalPointY - self.shift != ChessWareHouse.shared.chessBoardWidth {
            if isDoubleShift(view: currentView, originalPointX: originalPointX, originalPointY: originalPointY) || isSingleShift(view: currentView, originalPointX: originalPointX, originalPointY: originalPointY) {
//                currentView.backgroundColor = .red
//                currentView.alpha = 0.6
                return true
            }
        } else if originalPointY - self.shift == 0 || originalPointY - self.shift == ChessWareHouse.shared.chessBoardWidth {
            if isWhitePiece() {
                if isSingleWhiteShift(view: currentView, originalPointX: originalPointX, originalPointY: originalPointY) {
                    return true
                }
            } else {
                if isSingleShift(view: currentView, originalPointX: originalPointX, originalPointY: originalPointY) {
                    return true
                }
            }
    } else {
            if isWhitePiece() {
                if isSingleWhiteShift(view: currentView, originalPointX: originalPointX, originalPointY: originalPointY) {
                    return true
                }
            } else {
                if isSingleShift(view: currentView, originalPointX: originalPointX, originalPointY: originalPointY) {
                    return true
                }
            }
        }
        return false
    }
    

    
    func isCurrentPieceStep(x: CGFloat, y: CGFloat, _ pieceCoordinate: (CGFloat, CGFloat)) -> Bool {
        return false
    }
    //------------------------------------------

//    func deselectViewArray(viewList: [UIView]) {
//        for currentView in viewList {
//            currentView.backgroundColor = .clear
//        }
//    }
//    
//    func selectViewArray(viewList: [UIView]) {
//        for currentView in viewList {
//            currentView.backgroundColor = .red
//            currentView.alpha = 0.6
//        }
//    }
    
    func isEndPoint(orignalView: UIView, currentView: UIView) -> Bool {
        print("Pawn-isEndPoint")
        self.shift = getShiftByColor(shift: ChessWareHouse.shared.chessBoardWidth! / 8)
        return (!isWhitePiece() && orignalView.frame.minY - self.shift == 0 ||
                    isWhitePiece() && orignalView.frame.minY - self.shift == ChessWareHouse.shared.chessBoardWidth! + self.shift) 
    }
    
    func isEatablePiece(capturedPiece: ChessBoardCell, currentPiece: ChessBoardCell) -> Bool {
        return (capturedPiece.boardCell == ChessBoardCellType.empty &&
                    !checkDiagonalMove(orignalView: currentPiece, currentView: capturedPiece)) ||
        (capturedPiece.boardCell == ChessBoardCellType.full && currentPiece.color != capturedPiece.color &&
            checkDiagonalMove(orignalView: currentPiece, currentView: capturedPiece))
    }
    
    func checkDiagonalMove(orignalView: UIView, currentView: UIView) -> Bool {
        return (orignalView.frame.minX == currentView.frame.minX + self.shift &&
                orignalView.frame.minY == currentView.frame.minY + self.shift) ||
            (orignalView.frame.minX == currentView.frame.minX - self.shift &&
                orignalView.frame.minY == currentView.frame.minY + self.shift)
            
    }
    //------------------------------------------
    func getPerhapsMoveArray(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat)) -> [UIView] {
        print("pawn getPerhapsMoveArray")
        var pieceShiftArray: [UIView] = []
        let originalV = findViewByСoordinate(orignalMinimalPoint.0, orignalMinimalPoint.1)
        if originalV.frame.width != 0.0 {
        let originalView = originalV as! ChessBoardCell
        sizeBoard = ChessWareHouse.shared.chessBoardWidth ?? 400.0

            for subview in ChessWareHouse.shared.chessPieceStorage {
              self.shift = getShiftByColor(shift: ChessWareHouse.shared.chessBoardWidth! / 8)
//                if orignalMinimalPoint.0 == 200 && orignalMinimalPoint.1 == 50 &&
//                    subview.frame.minX == 150 && subview.frame.minY == 0 {
//
//                    print("hey")
//                }
                if isPawnShift(currentView: subview, originalPointX: orignalMinimalPoint.0, originalPointY: orignalMinimalPoint.1) {
                    if isEatablePiece(capturedPiece: subview as! ChessBoardCell, currentPiece: originalView) {
                       pieceShiftArray.append(subview)
                    }
//                    if isEndPoint(orignalView: originalView, currentView: subview) {
//                        var view = subview as! ChessBoardCell
//                        view.nameCell = .queen
//                        pieceShiftArray = []
//                        return [view]
//                    }
              }
            }
        }
        return pieceShiftArray
    }
    
    //------------------------------------------
    
//    func isCorrectMove(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat)) -> Bool {
//        var pieceShiftArray: [UIView] = []
//        sizeBoard = ChessWareHouse.shared.chessBoardWidth ?? 400.0
//        
//        var finalView = findViewByСoordinate(finalPointX, finalPointY)
//
//        if !isExistViewByCoordinate(x: finalPointX, y: finalPointY, view: finalView) {
//            finalView.frame = CGRect(x: orignalMinimalPoint.0, y: orignalMinimalPoint.1, width: finalView.frame.maxX - finalView.frame.minX, height: finalView.frame.maxX - finalView.frame.minX)
//        } else {
//            pieceShiftArray = getPerhapsMoveArray(x: finalPointX, y: finalPointY, orignalMinimalPoint)
//        }
//        var isFound = false
//        for validView in pieceShiftArray {
//      
//        if isExistViewByCoordinate(x: finalPointX, y: finalPointY, view: validView) {
//            finalView.frame = CGRect(x: validView.frame.minX, y: validView.frame.minY, width: validView.frame.maxX - validView.frame.minX, height: validView.frame.maxX - validView.frame.minX)
//            isFound = true
//        }
//        }
//        if !isFound {
//                finalView.frame = CGRect(x: orignalMinimalPoint.0, y: orignalMinimalPoint.1, width: finalView.frame.maxX - finalView.frame.minX, height: finalView.frame.maxX - finalView.frame.minX)
//            }
//        return false
//    }
    //--------------------------------------------------------------

    
    func movePiece(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat), pieceShiftArray: [UIView]) {
        var finalView = findViewByСoordinate(finalPointX, finalPointY)
        var isFound = false
        for validView in pieceShiftArray {
      
        if isExistViewByCoordinate(x: finalPointX, y: finalPointY, view: validView) {
            finalView.frame = CGRect(x: validView.frame.minX, y: validView.frame.minY, width: validView.frame.maxX - validView.frame.minX, height: validView.frame.maxX - validView.frame.minX)
            isFound = true
        }
        }
        if !isFound {
                finalView.frame = CGRect(x: orignalMinimalPoint.0, y: orignalMinimalPoint.1, width: finalView.frame.maxX - finalView.frame.minX, height: finalView.frame.maxX - finalView.frame.minX)
            }
    }
    
    func findViewCoordinateByСoordinate(_ x: CGFloat, _ y: CGFloat) -> (CGFloat, CGFloat) {
        var view: UIView?
        var flag = 0
        for subview in ChessWareHouse.shared.chessPieceStorage {
            flag += 1
            if x > subview.frame.minX && y > subview.frame.minY &&
                x < subview.frame.maxX && y < subview.frame.maxY &&
                subview.frame.maxX - subview.frame.minX != ChessWareHouse.shared.chessBoardWidth {
                view = subview
            }
        }
        return (view!.frame.minX, view!.frame.minY)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
//    func isCorrectMove(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalMinimalPoint: (CGFloat, CGFloat)) {
//        var pieceShiftArray: [UIView]?
//        sizeBoard = ChessWareHouse.shared.chessBoardWidth ?? 400.0
//        for subview in ChessWareHouse.shared.chessPieceStorage {
//            if isExistViewByCoordinate(x: finalPointX, y: finalPointY, view: subview) {
//                print("Yes")
//            }
//            //1.check color of piece and set - or + to shift
//            //2.find all rightly view
//            //3.check invalid views
//
//         }
//    }
   
//    func isCurrentPieceStep(x finalPointX: CGFloat, y finalPointY: CGFloat, _ orignalPoint: (CGFloat, CGFloat)) -> Bool {
//        var flag = 0
//        var buffer: UIView?
//        var counter = 0
//        sizeBoard = ChessWareHouse.shared.chessBoardWidth ?? 400.0
////        print(ChessWareHouse.shared.chessPieceStorage)
//        for subview in ChessWareHouse.shared.chessPieceStorage {
//            if counter > 51 {
//                print(subview)
////                print(x)
////                print(y)
////                print(pieceCoordinate.0)
////                print(pieceCoordinate.1)
//            }
//            counter += 1
//            var orignalMininimalPoint = findViewByСoordinate(orignalPoint.0, orignalPoint.1)
//            var finalMininimalPoint = findViewByСoordinate(finalPointX, finalPointY)
//            let distancePoint = getShiftByPoint(originalPointX: orignalPoint.0, originalPointY: orignalPoint.1, finalPointX: finalPointX, finalPointY: finalPointY,  view: subview)
//
////            print(subview)
//
//
//
//            if isAlreadyPlacedPiece(x: finalPointX, y: finalPointY, view: subview) && isBoardView(view: subview) {
//                print("Bug")
//                if !isPawnMove(originalPointX: Double(finalPointX), originalPointY: Double(finalPointY), finalPointX: Double(orignalPoint.0), finalPointY: Double(orignalPoint.1), shift: ([25.0, 25], 0)) {
//                    print("in isPawnMove")
////                    print(subview)
//                    if !isOriginalCoordinate(pieceMinX: Double(subview.frame.minX), pieceMinY: Double(subview.frame.minY), distancePoint: distancePoint.0[0]) {
//
//                        print("in isOriginalCoordinate")
//                        subview.frame = CGRect(x: orignalPoint.0, y: orignalPoint.1, width: subview.frame.maxX - subview.frame.minX, height: subview.frame.maxX - subview.frame.minX)
//                    }
//            }
//            } else if isExistViewByCoordinate(x: finalPointX, y: finalPointY, view: subview) &&
//                isBoardView(view: subview) {
//                var val = findViewByСoordinate(finalPointX, finalPointY)
//                print("isExistViewByCoordinate")
//                if isPawnMove(originalPointX: Double(finalPointX), originalPointY: Double(finalPointY), finalPointX: Double(orignalPoint.0), finalPointY: Double(orignalPoint.1), shift: distancePoint) {
////                    print("Pawn")
//
//                    subview.frame = CGRect(x: val.0, y: val.1, width: subview.frame.maxX - subview.frame.minX, height: subview.frame.maxX - subview.frame.minX)
////                    if isOriginalCoordinate(pieceMinX: pieceMinX, pieceMinY: pieceMinY, distancePoint: Double(distancePoint.0[0])) {
////                    subview.frame = CGRect(x: CGFloat(buffer?.frame.minX ?? 0.0), y: CGFloat(buffer?.frame.minY ?? 0.0), width: subview.frame.maxX - subview.frame.minX, height: subview.frame.maxX - subview.frame.minX)
////                } else {
////                    buffer?.frame = CGRect(x: Double(subview.frame.minX), y: Double(subview.frame.minY), width: abs(distancePoint.0[0]), height: abs(distancePoint.0[0]))
////                }
//                flag = 0
//                }
////                else {
////                    if !isOriginalCoordinate(pieceMinX: pieceMinX, pieceMinY: pieceMinY, distancePoint: distancePoint.0[0]) {
////                        subview.frame = CGRect(x: pieceCoordinate.0, y: pieceCoordinate.1, width: subview.frame.maxX - subview.frame.minX, height: subview.frame.maxX - subview.frame.minX)
////                    }
////                }
//
////                if flag == 0 {
////                    buffer = subview
////                    flag += 1
////                } else if flag == 1 {
////                    let pieceMinX = Double(buffer?.frame.minX ?? 0.0)
////                    let pieceMinY = Double(buffer?.frame.minY ?? 0.0)
////
////                    if isPawnMove(originalPointX: Double(x), originalPointY: Double(y), finalPointX: Double(pieceCoordinate.0), finalPointY: Double(pieceCoordinate.1), shift: distancePoint) {
//////                    print("Pawn")
////                        if isOriginalCoordinate(pieceMinX: pieceMinX, pieceMinY: pieceMinY, distancePoint: Double(distancePoint.0[0])) {
////                        subview.frame = CGRect(x: CGFloat(buffer?.frame.minX ?? 0.0), y: CGFloat(buffer?.frame.minY ?? 0.0), width: subview.frame.maxX - subview.frame.minX, height: subview.frame.maxX - subview.frame.minX)
////                    } else {
////                        buffer?.frame = CGRect(x: Double(subview.frame.minX), y: Double(subview.frame.minY), width: abs(distancePoint.0[0]), height: abs(distancePoint.0[0]))
////                    }
////                    flag = 0
////                    } else {
//////                        print("piece back")
////                        if !isOriginalCoordinate(pieceMinX: pieceMinX, pieceMinY: pieceMinY, distancePoint: distancePoint.0[0]) {
////                            subview.frame = CGRect(x: pieceCoordinate.0, y: pieceCoordinate.1, width: subview.frame.maxX - subview.frame.minX, height: subview.frame.maxX - subview.frame.minX)
////                        }
////                    }
////                }
//            }
//        }
//        return true
//    }
    
//    func isPawnMove(originalPointX: Double, originalPointY: Double, finalPointX: Double, finalPointY: Double, shift: ([Double], Double)) -> Bool {
//        var shiftBuffer = 0.0
//        if shift.0.count > 50 {
//            shiftBuffer = shift.0[0]
//        } else if finalPointY - originalPointY > 50 {
//            shiftBuffer = shift.0[0]
//        } else if finalPointY - originalPointY < 50 {
//            shiftBuffer = shift.0[0]
//        } else {
//            shiftBuffer = shift.0[0]//1
//        }
////        if shift < 0 {
////        if !(finalPointY + shift.0[0] < originalPointY && finalPointX + shift.1 < originalPointX) {
////            return finalPointY + shift.0[1] < originalPointY && finalPointX + shift.1 < originalPointX
////        }
//        return finalPointY + shiftBuffer * 1.2 < originalPointY && finalPointX + shift.1 < originalPointX
////        }
////        return finalPointY + shift.0 > originalPointY && finalPointX + shift.1 > originalPointX
//    }
    
//    func isRightPieceStep(originalPointX: CGFloat, originalPointY: CGFloat, finalPointX: CGFloat, finalPointY: CGFloat,  view: UIView) -> CGFloat {
//        if finalPointX > originalPointX && finalPointY > originalPointY {
//            return -view.frame.width
//        }
//        return view.frame.width
//    }
    func getShiftByPoint(originalPointX: CGFloat, originalPointY: CGFloat, finalPointX: CGFloat, finalPointY: CGFloat,  view: UIView) -> ([Double], Double) {
        var sizePieceStep: [Double] = []
        if originalPointY + (view.frame.maxX - view.frame.minX) / 2 == sizeBoard / 4 {
            sizePieceStep.append(Double(view.frame.width * 2))
            sizePieceStep.append(Double(view.frame.width))
        } else if originalPointY + (view.frame.maxX - view.frame.minX) / 2 == sizeBoard - sizeBoard / 4 {
            sizePieceStep.append(Double(-view.frame.width * 2))
            sizePieceStep.append(Double(-view.frame.width))
        } else {
            sizePieceStep.append(Double(view.frame.width))
        }
        if originalPointX >= finalPointX && !(originalPointY >= finalPointY) || originalPointX >= finalPointX && !(originalPointY <= finalPointY) {
            
            return (sizePieceStep, 0.0)
        } else if originalPointX <= finalPointX && !(originalPointY >= finalPointY) || originalPointX <= finalPointX && !(originalPointY <= finalPointY) {
            return (sizePieceStep, 0.0)
        }
        return ([0.0], 0.0)
    }
    
    func isOriginalCoordinate(pieceMinX: Double, pieceMinY: Double, distancePoint: Double) -> Bool {
        return pieceMinX.truncatingRemainder(dividingBy: abs(distancePoint)) == 0 && pieceMinY.truncatingRemainder(dividingBy: abs(distancePoint)) == 0
    }
    
    func getPieceStepSizeByColor(view: UIView) -> CGFloat {
        if color == ChessPieceColor.white {
            return view.frame.width
        }
        return -view.frame.width
    }
}
//    func isExistRechargeDoubleShift(nearByPoint: (CGFloat, CGFloat), originalPoint: (CGFloat, CGFloat), currentView: UIView) -> Bool {
//        if sizeBoard - sizeBoard * 3 / 8 == currentView.frame.minY && isNearByPieceForRecharge(nearByPoint: nearByPoint, originalPoint: originalPoint, currentView: currentView) {
//
//        } else if sizeBoard / 2 == currentView.frame.minY && isNearByPieceForRecharge(nearByPoint: nearByPoint, originalPoint: originalPoint, currentView: currentView) {
//
//        }
//        return false
//    }
    
//    func isNearByPieceForRecharge(nearByPoint: (CGFloat, CGFloat), originalPoint: (CGFloat, CGFloat), currentView: UIView) -> Bool {
//        return nearByPoint.1 - originalPoint.1 >= currentView.frame.maxY - currentView.frame.minY || nearByPoint.1 - originalPoint.1 >= currentView.frame.minY - currentView.frame.maxY
//    }
    
//    func isRightPieceShift(originalPoint: (CGFloat, CGFloat), currentView: UIView) -> Bool {
//        if sizeBoard - sizeBoard / 4 == currentView.frame.minY {
//
//        } else if sizeBoard / 4 == currentView.frame.minY {
//
//        } else {
//
//        }
//        return false
//    }
