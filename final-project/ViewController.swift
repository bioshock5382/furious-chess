//
//  ViewController.swift
//  final-project
//
//  Created by Roman Alikevich on 04.04.2021.
//

import UIKit

class ViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var boardView: UIView!
    @IBOutlet weak var chessBoardView: UIImageView!
    var pieceFactory: ChessPieceFactory?
    var pieceManager: ChessPieceManager?
    var piece: ChessPiece?
    let panRecognizer = UIPanGestureRecognizer()
    let tapRecognizer = UITapGestureRecognizer()
    var chessBoardCellArray: [[ChessBoardCellEntity]] = []
    var pieceArray: [String] = ["r", "n", "b", "q", "k", "b", "n", "r"]
    var pieceProspectiveMoveSet: [String: [UIView]] = [:]
    var viewWidht:CGFloat = 0
    var viewHeight:CGFloat = 0
    var chessBoardSize = 8
    var x :CGFloat = 0
    var y :CGFloat = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        let tapRecognizer = UITapGestureRecognizer()
        //        tapRecognizer.delegate = self
        //        tapRecognizer.addTarget(self, action: #selector(tapHandler(_:)))
        //        self.view.addGestureRecognizer(tapRecognizer)
        
        viewWidht = self.boardView.frame.width / 8
        viewHeight = self.boardView.frame.height / 8
        //        ChessWareHouse.shared.chessPieceStorage = boardView.subviews
        ChessWareHouse.shared.chessBoardWidth = boardView.frame.width
        self.view.backgroundColor = UIColorFromRGB(rgbValue: 0x312E2B)
        
        for row in 0..<chessBoardSize {
            var rowArray: [ChessBoardCellEntity] = []
            for cell in 0..<chessBoardSize {
                if row == 0 {
                    let img = UIImage(named: "w" + pieceArray[cell])
                    rowArray.append(ChessBoardCellEntity(pieceImage: img, pieceName: PieceEntity(rawValue: pieceArray[cell]), color: ChessPieceColor.white, boardCell: .full))
                } else if row == 1 {
                    rowArray.append(ChessBoardCellEntity(pieceImage: UIImage(named: "wp"), pieceName: PieceEntity(rawValue: "p"), color: ChessPieceColor.white, boardCell: .full))
                } else if row == 6 {
                    rowArray.append(ChessBoardCellEntity(pieceImage: UIImage(named: "bp"), pieceName: PieceEntity(rawValue: "p"), color: ChessPieceColor.black, boardCell: .full))
                } else if row == 7 {
                    rowArray.append(ChessBoardCellEntity(pieceImage: UIImage(named: "b" + pieceArray[cell]), pieceName: PieceEntity(rawValue: pieceArray[cell]), color: ChessPieceColor.black, boardCell: .full))
                } else {
                    rowArray.append(ChessBoardCellEntity(pieceImage: nil, pieceName: PieceEntity(rawValue: "empty"), color: ChessPieceColor.transparent, boardCell: .empty))
                }
            }
            chessBoardCellArray.append(rowArray)
        }
        for row in 0..<chessBoardSize {
            
            if x + viewWidht > boardView.frame.width {
                x = 0 
                y += viewHeight
            }
            
            for cell in 0..<chessBoardSize {
                let layout = UINib(nibName: "ChessBoardCell", bundle: Bundle.main)
                if let cellView = layout.instantiate(withOwner: chessBoardView, options: nil).first as? ChessBoardCell {
                    cellView.frame = CGRect(x: x, y: y, width: viewWidht, height: viewHeight)
                    cellView.addCell(chessBoardCellArray[row][cell].pieceImage, PieceEntity(rawValue: chessBoardCellArray[row][cell].pieceName?.rawValue ?? PieceEntity.empty.rawValue),
                                     color: chessBoardCellArray[row][cell].color,
                                     boardCell: chessBoardCellArray[row][cell].boardCell)
                    cellView.backgroundColor = .clear
                    
//                    let pieceFactory = ChessPieceFactory(color: chessBoardCellArray[row][cell].color)
//                    let pieceManager = ChessPieceManager(pieceFactory: pieceFactory ?? ChessPieceFactory(color: chessBoardCellArray[row][cell].color))
//                    let piece: ChessPiece = pieceManager.getPieceByValue(pieceType: chessBoardCellArray[row][cell].pieceName ?? PieceEntity.empty) ?? InvalidChessPieceError()
                    
                    movePiece(currentCell: cellView)
                    
                    let tapRecognizer = UITapGestureRecognizer(target: cellView, action: #selector(cellView.tapHandler(_:)))
                    cellView.addGestureRecognizer(tapRecognizer)
                
                    onTapPiece(currentCell: cellView)
                    
                    
                    if cellView.frame.origin.y + cellView.frame.height <= boardView.frame.height {
                        boardView.addSubview(cellView)
                        x += cellView.frame.width
                    } else {
                        x = 0
                        y = 0
                    }
                }
            }
        }
    }
    
    func movePiece(currentCell: ChessBoardCell) {
        let tapedView = currentCell as! ChessBoardCell
        pieceFactory = ChessPieceFactory(color: tapedView.color)
        pieceManager = ChessPieceManager(pieceFactory: pieceFactory ?? ChessPieceFactory(color: tapedView.color))
        piece = pieceManager?.getPieceByValue(pieceType: tapedView.nameCell ?? PieceEntity.empty) ?? InvalidChessPieceError()
        
        currentCell.onStartGesture = { [weak self] (x1, y1) in
            guard let self = self else { return }
            let originalPoint = self.findViewByСoordinate(x1, y1)
            print(x1)
            print(y1)
            currentCell.onPanGesture = { (x2, y2) in
                ChessWareHouse.shared.chessPieceStorage = self.boardView.subviews
                print(x2)
                print(y2)
                self.piece?.isCorrectMove(x: x2, y: y2, (originalPoint.0, originalPoint.1))
                //                        piece.movePiece(x: x1, y: y1)
            }
        }
    }
    
    func onTapPiece(currentCell: ChessBoardCell) {
        currentCell.onTapPieceGesture = { [weak self] in
            guard let self = self else { return }
            let cell = currentCell as? ChessBoardCell
           
            
            let tapedView = currentCell as! ChessBoardCell
            self.pieceFactory = ChessPieceFactory(color: tapedView.color)
            self.pieceManager = ChessPieceManager(pieceFactory: self.pieceFactory ?? ChessPieceFactory(color: tapedView.color))
            self.piece = self.pieceManager?.getPieceByValue(pieceType: tapedView.nameCell ?? PieceEntity.empty) ?? InvalidChessPieceError()
            ChessWareHouse.shared.chessPieceStorage = self.boardView.subviews
            ChessWareHouse.shared.boardView = self.boardView
            print(currentCell.frame.minX)
            print(currentCell.frame.minY)
            let index = "originalPointX:\(currentCell.frame.minX):originalPointY:\(currentCell.frame.minY)"
            let viewList = self.piece?.getPerhapsMoveArray(x: 0.0, y: 0.0, (currentCell.frame.minX, currentCell.frame.minY))
            //-----------
            //-----------
            print(viewList)
            //            var originalMinPoint: (Double, Double)
            if self.pieceProspectiveMoveSet.isEmpty {
                guard ChessWareHouse.shared.succeedMove == cell?.color && cell?.boardCell != ChessBoardCellType.empty else {
                    return
                }
                self.pieceProspectiveMoveSet[index] = viewList
                self.piece?.selectViewArray(viewList: viewList ?? [UIView()])
                currentCell.backgroundColor = .brown
                
            } else {
                var flag = 0
                for pieceElement in self.pieceProspectiveMoveSet.enumerated() {
                    if pieceElement.element.key == index {
                        self.piece?.deselectViewArray(viewList: viewList ?? [UIView()])
                        self.pieceProspectiveMoveSet.removeValue(forKey: index)
                        flag += 1
                        currentCell.backgroundColor = .clear
                    } else {
                        for val in pieceElement.element.value {
                            if currentCell.frame.minX == val.frame.minX &&
                                currentCell.frame.minY == val.frame.minY {
                                let originalPointArray: [String] = pieceElement.element.key.components(separatedBy: ":")
                                var originalView: ChessBoardCell? = nil
                                if let originalPointXBuf = NumberFormatter().number(from: originalPointArray[1]),
                                   let originalPointYBuf = NumberFormatter().number(from: originalPointArray[3]) {
                                    originalView = self.piece?.findViewByСoordinate(CGFloat(originalPointXBuf), CGFloat(originalPointYBuf)) as! ChessBoardCell
                                    let pieceView = val as! ChessBoardCell
                                    let finalPointX = val.frame.minX
                                    let finalPointY = val.frame.minY
                                    let viewColor = originalView!.color
                                    let pieceType = originalView!.nameCell
                                    if pieceView.boardCell == ChessBoardCellType.full {
                                        val.removeFromSuperview()
                                    } else {
                                        val.frame = CGRect(x: originalView!.frame.minX, y: originalView!.frame.minY, width: originalView!.frame.maxX - originalView!.frame.minX, height: originalView!.frame.maxX - originalView!.frame.minX)
                                    }
                                    let pieceFactory = ChessPieceFactory(color: viewColor)
                                    let pieceManager = ChessPieceManager(pieceFactory: pieceFactory ?? ChessPieceFactory(color: viewColor))
                                    let pieceCurrent: ChessPiece = pieceManager.getPieceByValue(pieceType: pieceType ?? PieceEntity.empty) ?? InvalidChessPieceError()

                                    if pieceCurrent.isEndPoint(orignalView: originalView ?? UIView(), currentView: currentCell) {
                                        var pieceName = ""
                                        if !self.piece!.isWhitePiece() {
                                            pieceName = "wq"
                                        } else {
                                            pieceName = "bq"
                                        }
                                        
                                        originalView?.nameCell = PieceEntity.queen
                                        originalView?.boardPieceImageView.image = UIImage(named: pieceName)
                                        print("Queen!")
                                    }
                                    originalView?.frame = CGRect(x: finalPointX, y: finalPointY, width: originalView!.frame.maxX - originalView!.frame.minX, height: originalView!.frame.maxX - originalView!.frame.minX)
                                    self.piece?.deselectViewArray(viewList: pieceElement.element.value)
                                    originalView?.backgroundColor = .clear
                                    if originalView?.color == ChessPieceColor.white {
                                        ChessWareHouse.shared.succeedMove = ChessPieceColor.black
                                    } else {
                                        ChessWareHouse.shared.succeedMove = ChessPieceColor.white
                                    }
                                    self.pieceProspectiveMoveSet.removeValue(forKey: pieceElement.element.key)
                                    flag += 1
                                }
                                
                                //                                originalView?.frame = CGRect(x: val.frame.minX, y: val.frame.minY, width: val.frame.maxX - val.frame.minX, height: val.frame.maxX - val.frame.minX)
                                //                                piece.deselectViewArray(viewList: pieceElement.element.value)
                                //                                self.pieceProspectiveMoveSet.removeValue(forKey: pieceElement.element.key)
                                //                                flag += 1
                               
                            }
                        }
                    }
                }
            }
        }
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    
    func findViewByСoordinate(_ x: CGFloat, _ y: CGFloat) -> (CGFloat, CGFloat) {
        var view: UIView?
        var flag = 0
        
        for subview in boardView.subviews {
            flag += 1
            if x > subview.frame.minX && y > subview.frame.minY &&
                x < subview.frame.maxX && y < subview.frame.maxY &&
                subview.frame.maxX - subview.frame.minX != boardView.frame.width {
                view = subview
                print(view)
                //                return (subview.frame.minX, subview.frame.minY)
            }
        }
        print(flag)
        return (view!.frame.minX, view!.frame.minY)
    }
    
    func findViewByMinimalPoint(pointX: CGFloat, pointY: CGFloat) -> UIView {
        var viewArray = boardView.subviews.map({(view) in view as! ChessBoardCell})
        for subview in viewArray {
            if subview.frame.minX == pointX &&
                subview.frame.minY == pointY {
                return subview
            }
        }
        return UIView()
    }
    
    //    @objc func tapHandler(_ tapGestureRecognizer: UITapGestureRecognizer? = nil) {
    //        var currentPoint = tapGestureRecognizer?.location(in: boardView)
    //        let originalPoint = self.findViewByСoordinate(currentPoint!.x, currentPoint!.y)
    //        var index = "originalPointX:\(originalPoint.0)originalPointY\(originalPoint.1)"
    //        ChessWareHouse.shared.chessPieceStorage = self.boardView.subviews
    //        for row in 0..<chessBoardSize {
    //        for cell in 0..<chessBoardSize {
    //
    //            if chessBoardCellArray[row][cell].boardCell == ChessBoardCellType.full {
    //                pieceFactory = ChessPieceFactory(color: chessBoardCellArray[row][cell].color)
    //                pieceManager = ChessPieceManager(pieceFactory: pieceFactory ?? ChessPieceFactory(color: chessBoardCellArray[row][cell].color))
    //                let piece: ChessPiece = pieceManager?.getPieceByValue(pieceType: chessBoardCellArray[row][cell].pieceName ?? PieceEntity.empty) ?? InvalidChessPieceError()
    ////                piece.isCorrectMove(x: 0.0, y: 0.0, (originalPoint.0, originalPoint.1))
    //                var viewList = piece.getPerhapsMoveArray(x: 0.0, y: 0.0, (originalPoint.0, originalPoint.1))
    //
    //                if pieceProspectiveMoveSet.isEmpty {
    //                    self.pieceProspectiveMoveSet[index] = viewList
    //                }
    //
    //                for pieceElement in pieceProspectiveMoveSet.enumerated() {
    //                    if pieceElement.element.key == index {
    //                        piece.deselectViewArray(viewList: viewList)
    //                        self.pieceProspectiveMoveSet.removeValue(forKey: index)
    //                    }
    //                }
    ////                if self.pieceProspectiveMoveSet.contains(where: pieceProspectiveMoveSet[index])
    ////                {
    ////                    piece.deselectViewArray(viewList: viewList)
    ////                    self.pieceProspectiveMoveSet.removeValue(forKey: index)
    ////                } else {
    ////                    self.pieceProspectiveMoveSet[index] = viewList
    ////                }
    //            }
    //        }
    //        }
    ////        print(originalPoint)
    //    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}


