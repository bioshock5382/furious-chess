//
//  ChessPieceFactory.swift
//  final-project
//
//  Created by Roman Alikevich on 09.04.2021.
//

import UIKit

class ChessPieceFactory {
    
    var pieceColor: ChessPieceColor?
    
    init(color: ChessPieceColor?) {
        self.pieceColor = color
    }
    
     func getValue(piece: PieceEntity) -> ChessPiece  {
        switch piece {
        case .pawn: return Pawn(color: pieceColor)
        case .bishop: return Bishop(color: pieceColor)
        case .rook: return Rook(color: pieceColor)
        case .queen: return Queen(color: pieceColor)
        case .king: return King(color: pieceColor)
        case .knight: return Knight(color: pieceColor)
        case .invalidValue: return InvalidChessPieceError()
        case .empty: return Empty()
        }
//        return InvalidChessPieceError()
     }
}
