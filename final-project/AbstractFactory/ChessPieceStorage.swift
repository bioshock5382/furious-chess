//
//  ChessPieceStorage.swift
//  final-project
//
//  Created by Roman Alikevich on 09.04.2021.
//

import UIKit

class ChessPieceManager {
    let pieceFactory: ChessPieceFactory?
    
    init(pieceFactory: ChessPieceFactory) {
        self.pieceFactory = pieceFactory
    }
    
    func getPieceByValue(pieceType: PieceEntity) -> ChessPiece {
        return pieceFactory?.getValue(piece: pieceType) ?? InvalidChessPieceError()
    }
}
