//
//  ChessBoardCell.swift
//  final-project
//
//  Created by Roman Alikevich on 04.04.2021.
//

import UIKit

class ChessBoardCell: UIView, UIGestureRecognizerDelegate {
    @IBOutlet weak var boardPieceImageView: UIImageView!
    var nameCell: PieceEntity?
    var color: ChessPieceColor?
    var boardCell: ChessBoardCellType?
    let panGestureRecognizer = UIPanGestureRecognizer()
    var onPanGesture: ((CGFloat, CGFloat) ->())? = nil
    var onStartGesture: ((CGFloat, CGFloat) ->())? = nil
    var onTapPieceGesture: (() -> ())? = nil
    
    override func awakeFromNib() {
        let panRecognizer = UIPanGestureRecognizer()
        panRecognizer.delegate = self
        panRecognizer.addTarget(self, action: #selector(panHandler(_:)))
        self.addGestureRecognizer(panRecognizer)
        
//        let tapRecognizer = UITapGestureRecognizer()
//        tapRecognizer.delegate = self
//        tapRecognizer.addTarget(self, action: #selector(tapHandler(_:)))
//        self.addGestureRecognizer(tapRecognizer)
    }
    
    @objc func panHandler(_ gestureRecognizer: UIPanGestureRecognizer? = nil) {
        switch gestureRecognizer?.state ?? .failed {
        case .began:
            print("began")
            if let recognizer = gestureRecognizer {
                let translation = recognizer.translation(in: self)
            if let view = recognizer.view {
//                    print(view.center.x + translation.x)
//                    print(view.center.y + translation.y)
                if let action = onStartGesture {
                    action(view.center.x + translation.x, view.center.y + translation.y)
                }
            }
            }
            self.alpha = 0.6
        case .changed:
            if let recognizer = gestureRecognizer {
                let translation = recognizer.translation(in: self)
                if let view = recognizer.view {
                    view.center = CGPoint(x: view.center.x + translation.x, y: view.center.y + translation.y)
                }
                recognizer.setTranslation(CGPoint.zero, in: self)
            }
        case .cancelled, .ended:
            print("finished")
            if let recognizer = gestureRecognizer {
                let translation = recognizer.translation(in: self)
            if let view = recognizer.view {
//                print(view.center.x + translation.x)
//                print(view.center.y + translation.y)
                if let action = onPanGesture {
                    action(view.center.x + translation.x, view.center.y + translation.y)
                }
            }
            }
            self.alpha = 1
//            panGestureRecognizer = nil
            break
        case .failed, .possible: break
            break
    }
}
    
    @objc func tapHandler(_ tapGestureRecognizer: UITapGestureRecognizer? = nil) {
//        var currentPoint = tapGestureRecognizer?.location(in: self)
        print("tapHandler")
        if let action = onTapPieceGesture {
            action()
        }
//        print(currentPoint)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func addCell(_ image: UIImage?, _ nameCell: PieceEntity?, color: ChessPieceColor?, boardCell: ChessBoardCellType?) {
        self.nameCell = nameCell
        self.color = color
        self.boardCell = boardCell
        self.boardPieceImageView.image = image
    }
}
