//
//  CustomGestureRecognizer.swift
//  final-project
//
//  Created by Roman Alikevich on 07.04.2021.
//

import UIKit

class CustomGestureRecognizer : NSObject, UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
