//
//  ChessWareHouse.swift
//  final-project
//
//  Created by Roman Alikevich on 11.04.2021.
//

import UIKit

class ChessWareHouse {
    static var shared = ChessWareHouse()
    var chessPieceStorage: [UIView] = []
    var boardView: UIView? = nil
    var chessBoardWidth: CGFloat? = 0.0
    var succeedMove: ChessPieceColor? = .white
    private init() {}
}
